package com.practice.lms.dao;

import com.practice.lms.bean.A;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ADao {
A selectByPrimaryKey(Integer id);
}
