package com.practice.lms.controller;

import com.practice.lms.bean.A;
import com.practice.lms.service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AController {

@Autowired
AService aService;

@GetMapping("/selectByPrimaryKey")
@ResponseBody
public A selectByPrimaryKey(Integer id){
return aService.selectByPrimaryKey(id);
}
}
