package com.practice.lms.service.impl;

import com.practice.lms.bean.A;
import com.practice.lms.dao.ADao;
import com.practice.lms.service.AService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AServiceImpl implements AService {

@Autowired
ADao aDao;

@Override
public A selectByPrimaryKey(Integer id) {
return aDao.selectByPrimaryKey(id);
}
}
