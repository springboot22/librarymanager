package com.practice.lms.service.iservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.practice.lms.entity.Role;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:01
 */
@Service
public interface RoleService extends IService<Role> {
}
