package com.practice.lms.service.iservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.practice.lms.entity.UserAuthority;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:02
 */
@Service
public interface UserAuthorityService extends IService<UserAuthority> {
}
