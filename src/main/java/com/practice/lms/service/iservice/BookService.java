package com.practice.lms.service.iservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.practice.lms.entity.Book;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:00
 */
@Service
public interface BookService extends IService<Book> {
}
