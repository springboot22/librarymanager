package com.practice.lms.service.iservice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.practice.lms.entity.Users;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 8:58
 */
@Service
public interface UsersService extends IService<Users> {
}
