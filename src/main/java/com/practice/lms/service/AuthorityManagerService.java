package com.practice.lms.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.practice.lms.annocation.Mylog;
import com.practice.lms.common.AuthorityEnum;
import com.practice.lms.common.AuthorityStatus;
import com.practice.lms.common.RoleEnum;
import com.practice.lms.entity.UserAuthority;
import com.practice.lms.entity.UserRole;
import com.practice.lms.entity.Users;
import com.practice.lms.service.impl.RoleServiceImpl;
import com.practice.lms.service.impl.UserAuthorityServiceImpl;
import com.practice.lms.service.impl.UserRoleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author will.tuo
 * @date 2021/8/20 10:24
 *
 * 權限管理相關服務
 */
@Slf4j
@Service
public class AuthorityManagerService {
    @Autowired
    UserManagerService userManagerService;
    @Autowired
    UserRoleServiceImpl userRoleService;
    @Autowired
    RoleServiceImpl roleService;
    @Autowired
    UserAuthorityServiceImpl userAuthorityService;
    /**
     * 查询用户权限集合
     * 这里需要将role 和 authority翻译成一个集合。
     * @return
     */
    public List<AuthorityEnum> getAuthoritiesForUser(String username){
        Users user = userManagerService.selectUserByUsername(username);
//        log.info(userAuthorityService.toString());
        List<UserAuthority> userAuthorities = userAuthorityService.list().stream()
                .filter(w->w.getUserId() == user.getId())
                .filter(w1-> !w1.getAuthStatus().equals(AuthorityStatus.FORBIDDEN.getStatus()))
                .collect(Collectors.toList());
//        log.info(userAuthorities.toString());
//        log.info(userAuthorities.stream().map((e)-> AuthorityEnum.getAuthorityById(e.getAuthorityId())).collect(Collectors.toList()).toString());
        return userAuthorities.stream()
                .map((e)-> AuthorityEnum.getAuthorityById(e.getAuthorityId()))
                .collect(Collectors.toList());
    }

    /**
     * 用户角色查询
     * @param username
     * @return
     */
    public RoleEnum getUserRole(String username){
        Users user = userManagerService.selectUserByUsername(username);
        UserRole userRole = userRoleService.list().stream().filter(w->w.getUserId()==user.getId()).findFirst().get();
        return RoleEnum.getRoleEnum(userRole.getRoleId());
    }

    /**
     * 设置用户角色
     * @return
     */
    public boolean setUserRole(String username,int roleid){
        Users user = userManagerService.selectUserByUsername(username);
        UpdateWrapper<UserRole> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("role_id",roleid).eq("user_id",user.getId());
        return userRoleService.update(updateWrapper);
    }
    /**
     * 添加用户权限
     * @param username
     * @return
     */
    public boolean addUserAuthority(String username, int authorityid){
        Users user = userManagerService.selectUserByUsername(username);
        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setUserId(user.getId());
        userAuthority.setAuthorityId(authorityid);
        userAuthority.setAuthStatus(AuthorityStatus.ADD.getStatus());
        return userAuthorityService.save(userAuthority);
    }

    /**
     * 移除用户权限
     * @return
     */
    public boolean removeUserAuthority(String username, int authorityid){
        Users user = userManagerService.selectUserByUsername(username);
        LambdaQueryWrapper<UserAuthority> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.eq(UserAuthority::getUserId,user.getId())
                .eq(UserAuthority::getAuthorityId,authorityid);
        return userAuthorityService.remove(lambdaQueryWrapper);
    }

    /**
     * 禁用用户权限
     * 设置权限表里某个权限状态为 FORBIDDEN
     * @return
     */
    public boolean forbiddenUserAuthority(String username, int authorityid){
        Users user = userManagerService.selectUserByUsername(username);
        UpdateWrapper<UserAuthority> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("auth_status",AuthorityStatus.FORBIDDEN.getStatus())
                .eq("user_id",user.getId())
                .eq("authority_id",authorityid);
        return userAuthorityService.update(updateWrapper);
    }
    public String getAuthorityCollection(String userName){
        System.out.println("getAuthorityCollectiondsvsfvdfbsdbdsbdg");
        StringBuilder sb = new StringBuilder();


        String userRole = getUserRole(userName).getRole();
        log.info("vvvvvvvv"+userRole);
        sb.append("ABB,ROLE_");
        log.info("vvvvvvvv"+sb.toString());
        sb.append(userRole);
        sb.append(",");

        List<AuthorityEnum> as = getAuthoritiesForUser(userName);
        String ass = as.stream().map(AuthorityEnum::getAuthority).reduce((x1,x2)->x1+","+x2).get();
        sb.append(ass);
        log.info("vvvvvvvv"+sb.toString());
        return sb.toString();
    }

}
