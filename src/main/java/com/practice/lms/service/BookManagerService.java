package com.practice.lms.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.practice.lms.common.BookStatus;
import com.practice.lms.entity.Book;
import com.practice.lms.entity.Users;
import com.practice.lms.mapper.BookMapper;
import com.practice.lms.service.impl.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.EntityWriter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author will.tuo
 * @date 2021/8/20 11:09
 */
@Service
public class BookManagerService {
    @Autowired
    BookServiceImpl bookService;
    @Autowired
    BookMapper bookMapper;
    /**
     * 全部书籍列表
     * @return
     */
    public List<Book> selectAll(){
        return bookService.list();
    }
    /**
     * 关键字查询书籍列表
     * @param key
     * @return
     */
    public List<Book> selectBooksByKey(String key){
        QueryWrapper<Book> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("book_name",key);
        return bookService.list(queryWrapper);
    }

    /**
     * 借书
     * - 设置书籍状态
     * - 设置借阅人id
     * - 设置借阅时间
     * @param userid
     * @param bookid
     * @return
     */
    public boolean borrow(int userid,int bookid){
        try{
            UpdateWrapper<Book> updateWrapper = new UpdateWrapper<>();

            Book book = bookService.getById(bookid);
            book.setBookStatus(BookStatus.BORROWED.getStatus());
            /**
             * 借阅时间设置
             */
            book.setBorrowerId(userid);
            bookMapper.updateById(book);
            return true;
        }catch (Exception e){
        }
        return false;
    }

    /**
     * 还书
     * - 设置书籍状态
     * - 清除借阅人id
     * - 清除借阅时间
     * @param userid
     * @param bookid
     * @return
     */
    public boolean returnBook(int userid,int bookid){
        Book book = bookService.getById(bookid);
        book.setBookStatus(BookStatus.FREE.getStatus());
        book.setBorrowerId(-1);
        /**
         * 借阅时间清空
         */
        bookMapper.updateById(book);
        return true;
    }

    /**
     * - 保存书籍
     * - 设置书籍入馆实时间
     * - 设置书籍初始状态
     * @param book
     * @return
     */
    public boolean addBook(Book book){
        bookService.save(book);
        return true;
    }

    /**
     * 更改书籍信息
     * - 更改书籍状态
     * @return
     */
    public boolean updateBookStatus(int bookid,String bookStatus){
        Book book = bookService.getById(bookid);
        book.setBookStatus(bookStatus);
        bookMapper.updateById(book);
        return true;
    }
    /**
     * 真删除
     * @param bookid
     * @return
     */
    public boolean deleteBook(int bookid){
        return bookService.removeById(bookid);
    }
}
