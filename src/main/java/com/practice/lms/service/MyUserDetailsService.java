package com.practice.lms.service;

import com.practice.lms.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author will.tuo
 * @date 2021/8/18 14:20
 *
 * @Service("userDetailsService") 这里的名称和Security2Config ->configure->auth.userDetailsService(userDetailsService);参数名一样
 * 这里的验证属于登录时的验证
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserManagerService userManagerService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userManagerService.selectUserByUsername(username);
        if(users==null){
            //认证失败
            throw new UsernameNotFoundException("用户名不存在");
        }
        return new User(users.getUsername(),new BCryptPasswordEncoder().encode(users.getPassword()),new ArrayList<>());
    }
}
