package com.practice.lms.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.practice.lms.annocation.Mylog;
import com.practice.lms.common.AuthorityEnum;
import com.practice.lms.common.RoleEnum;
import com.practice.lms.entity.UserRole;
import com.practice.lms.entity.Users;
import com.practice.lms.mapper.UsersMapper;
import com.practice.lms.service.impl.RoleServiceImpl;
import com.practice.lms.service.impl.UserRoleServiceImpl;
import com.practice.lms.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author will.tuo
 * @date 2021/8/20 14:09
 */
@Service
@Slf4j
public class UserManagerService {
    @Autowired
    UserServiceImpl userService;
    @Autowired
    AuthorityManagerService ams;
    public Users selectUserByUsername(String username){
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        return userService.getOne(wrapper);
    }
    /**
     * 通过用户名，如果能查到这个用户，就存在，如果为空则 不存在
     * @param username
     * @return
     */
    public boolean isExistUser(String username){
        return selectUserByUsername(username) != null;
    }
}
