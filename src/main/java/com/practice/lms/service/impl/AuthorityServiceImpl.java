package com.practice.lms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.practice.lms.entity.Authority;
import com.practice.lms.mapper.AuthorityMapper;
import com.practice.lms.service.iservice.AuthorityService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:01
 */
@Service
public class AuthorityServiceImpl extends ServiceImpl<AuthorityMapper, Authority> implements AuthorityService {
}
