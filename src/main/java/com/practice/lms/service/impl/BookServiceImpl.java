package com.practice.lms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.practice.lms.entity.Book;
import com.practice.lms.mapper.BookMapper;
import com.practice.lms.service.iservice.BookService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:00
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {
}
