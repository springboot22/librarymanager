package com.practice.lms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.practice.lms.entity.UserAuthority;
import com.practice.lms.mapper.UserAuthorityMapper;
import com.practice.lms.service.iservice.UserAuthorityService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:02
 */
@Service
public class UserAuthorityServiceImpl extends ServiceImpl<UserAuthorityMapper, UserAuthority> implements UserAuthorityService {
}
