package com.practice.lms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.practice.lms.entity.Users;
import com.practice.lms.mapper.UsersMapper;
import com.practice.lms.service.iservice.UsersService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 8:59
 */
@Service
public class UserServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {


    public boolean register(Users users){
        if(!save(users)){

            return false;
        }else{

        }
        return true;
    }
}

