package com.practice.lms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.practice.lms.entity.Role;
import com.practice.lms.mapper.RoleMapper;
import com.practice.lms.service.iservice.RoleService;
import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/20 9:01
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
}
