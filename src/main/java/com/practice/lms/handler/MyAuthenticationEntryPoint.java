package com.practice.lms.handler;

import com.practice.lms.response.Result;
import com.practice.lms.response.ResultCode;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author will.tuo
 * @date 2021/8/19 13:03
 * 匿名用户访问无权限异常
 * 匿名 即 未登录
 */
@Component("myAuthenticationEntryPoint")
public class MyAuthenticationEntryPoint extends JSONAuthentication implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        Result result = null;

        result = Result.error(ResultCode.FAILURE.getCode()).message("用户未登录");
        this.WriteJSON(request,response,result);
    }
}
