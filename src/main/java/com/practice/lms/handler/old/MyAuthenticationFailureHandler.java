package com.practice.lms.handler.old;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import com.practice.lms.response.ResultCode;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author will.tuo
 * @date 2021/8/19 12:20
 */
@Component("myAuthenticationFailureHandler")
public class MyAuthenticationFailureHandler extends JSONAuthentication implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        System.out.println("验证失败处理器");
        Result result = null;
        result = Result.error(ResultCode.FAILURE.getCode()).message("验证失败");
        this.WriteJSON(request,response,result);

    }
}
