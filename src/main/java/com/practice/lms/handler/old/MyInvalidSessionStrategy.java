package com.practice.lms.handler.old;

import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author will.tuo
 * @date 2021/8/19 13:53
 * 会话超时 策略
 */
@Component("myInvalidSessionStrategy")
public class MyInvalidSessionStrategy extends JSONAuthentication implements InvalidSessionStrategy {
    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Result result = Result.ok().message("session超时");
        this.WriteJSON(request,response,result);
    }
}
