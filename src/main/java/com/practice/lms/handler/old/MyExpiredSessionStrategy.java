package com.practice.lms.handler.old;

import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author will.tuo
 * @date 2021/8/19 13:59
 * 当账户被踢下线的时候如何处理
 */
@Component("myExpiredSessionStrategy")
public class MyExpiredSessionStrategy extends JSONAuthentication implements SessionInformationExpiredStrategy {
    @Override
    public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
        Result result = Result.error(4042).message("你被挤下来了");
        this.WriteJSON(event.getRequest(),event.getResponse(),result);
    }
}
