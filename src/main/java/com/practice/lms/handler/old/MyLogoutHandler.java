package com.practice.lms.handler.old;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import com.practice.lms.response.ResultCode;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author will.tuo
 * @date 2021/8/19 13:19
 *
 * 注销操作
 */
@Component("myLogoutHandler")
public class MyLogoutHandler extends JSONAuthentication implements LogoutHandler {

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Result result = null;
        String headerToken = request.getHeader("Authorization");
        System.out.println(headerToken);
        if(!StringUtils.isEmpty(headerToken)){
            String token = headerToken.replace("Bearer","").trim();
            SecurityContextHolder.clearContext();
        }
    }
}
