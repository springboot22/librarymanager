package com.practice.lms.handler;

import com.practice.lms.utils.JwtUtil;
import com.practice.lms.utils.RedisUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author will.tuo
 * @date 2021/8/19 15:27
 * 登出 并清理缓存
 */
@Component("tokenLogoutHandler")
public class TokenLogoutHandler implements LogoutHandler {
    @Autowired
    RedisUtil redisUtil;
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String token = request.getHeader("token");
        if (token != null) {
            System.out.println();
            Claims claims = JwtUtil.getClaimsFromToken(token);
            assert claims != null;
            String userName = claims.getSubject();
            if(redisUtil.hasKey(userName)){
                redisUtil.del(userName);
            }
        }
    }
}
