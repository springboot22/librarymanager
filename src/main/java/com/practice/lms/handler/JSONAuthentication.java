package com.practice.lms.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author will.tuo
 * @date 2021/8/19 12:51
 * response 输出json封装
 */
@Component
public class JSONAuthentication {
    public void WriteJSON(HttpServletRequest request,
                          HttpServletResponse response,
                          Object data) throws IOException, ServletException{
        response.setContentType("application/json;charset=utf-8");
        response.setHeader("Access-COntrol-Allow-Origin","*");
        response.setHeader("Access-COntrol-Allow-Method","POST,GET");
        PrintWriter out = response.getWriter();
        out.write(new ObjectMapper().writeValueAsString(data));
        out.flush();
        out.close();
    }
}
