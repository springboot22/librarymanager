package com.practice.lms.aop;

import com.practice.lms.annocation.Mylog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author will.tuo
 * @date 2021/8/12 13:38
 *
 * 给方法加一个JoinPoint参数，就可以获取方法信息
 * 多个切面的话，默认会以切面类的字母顺序 先 后 原方法 后 先 执行
 * 或者定义一个Order注解
 * 环绕通知不会影响其他的切面，只是影响当前切面的几个通知的运行顺序
 */

@Aspect
@Component
@Slf4j
public class LogAspect {
    @Pointcut("@annotation(com.practice.lms.annocation.Mylog)")
    public void annotationPointCut() {};

    @Before("annotationPointCut()")
    public void after(JoinPoint joinPoint) {
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Method method = ms.getMethod();
        log.info(method.getName()+"正在被调用");
    }
    @Before("execution(* com.practice.lms.controller.*Controller.*(..))")
    public void before(JoinPoint joinPoint) {
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Method method = ms.getMethod();
        log.info("方法规则拦截" + method.getName());
    }
    /**
     *
     * @param joinPoint
     * @return
     */
    @Around("@annotation(com.practice.lms.annocation.Mylog)&&execution(* com.practice.lms.service.*Service.*(..))")
    public Object around(ProceedingJoinPoint joinPoint) {
        Object result = null;
        Object[] obs = joinPoint.getArgs();
        if(obs != null && obs.length >0) {
            String name = (String) obs[0];
            if("zhangsan".equals(name)) {
                try {
                    /**
                     * 前置
                     */
                    result = joinPoint.proceed();
                    /**
                     * 返回通知
                     */
                } catch (Throwable e) {
                    /**
                     * 异常通知
                     */
                    e.printStackTrace();
                } finally {
                    /**
                     * 后置通知
                     */
                }
            }else {
//                result =  "只有张三才允许修改";
            }
        }
        return result;
    }
}
