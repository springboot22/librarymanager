package com.practice.lms.response;

/**
 * @author will.tuo
 * @date 2021/8/19 11:24
 */
public interface CustomizeResultCode {
    /**
     * 获取错误状态码
     * @return
     */
    Integer getCode();

    /**
     * 获取错误信息
     * @return
     */
    String getMessaage();
}
