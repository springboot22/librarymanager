package com.practice.lms.response;

/**
 * @author will.tuo
 * @date 2021/8/19 11:23
 */
public enum ResultCode implements CustomizeResultCode {
    /**
     *
     */
    SUCCESS(200,"成功"),
    FAILURE(444,"失败");

    private Integer code;
    private String message;
    ResultCode(Integer code,String message){
        this.code = code;
        this.message = message;
    }
    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessaage() {
        return this.message;
    }
}
