package com.practice.lms.response;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author will.tuo
 * @date 2021/8/19 11:23
 */
@Data
public class Result {
    /**
     * 返回成功与否
     */
    private Boolean success;
    /**
     * 返回码
     */
    private Integer code;
    /**
     * 返回消息
     */
    private String message;
    /**
     * 返回数据体
     */
    private Map<String,Object> data = new HashMap<>();

    public Result(){}
    public static Result ok(){
        Result result = new Result();
        result.setSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMessage(ResultCode.SUCCESS.getMessaage());
        return result;
    }
    public static Result error(Integer resultCode){
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(resultCode);
        return result;
    }
    public Result message(String msg){
        this.setMessage(msg);
        return this;
    }
    public Result setDate(String key,Object value){
        this.data.put(key,value);
        return this;
    }
}
