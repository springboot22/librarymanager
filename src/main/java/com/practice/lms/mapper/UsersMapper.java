package com.practice.lms.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.practice.lms.entity.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2021/8/18 14:38
 */
@Repository
@Mapper
public interface UsersMapper extends BaseMapper<Users> {
}
