package com.practice.lms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.practice.lms.entity.Authority;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 */
@Mapper
@Repository
public interface AuthorityMapper extends BaseMapper<Authority> {
}
