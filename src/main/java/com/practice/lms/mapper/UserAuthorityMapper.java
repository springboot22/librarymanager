package com.practice.lms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.practice.lms.entity.UserAuthority;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2021/8/20 8:56
 */
@Mapper
@Repository
public interface UserAuthorityMapper extends BaseMapper<UserAuthority> {
}
