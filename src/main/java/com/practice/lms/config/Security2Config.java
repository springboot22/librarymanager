package com.practice.lms.config;

import com.practice.lms.common.RoleEnum;
import com.practice.lms.filter.MyUsernamePasswordLoginAuthenticationFilter;
import com.practice.lms.filter.TokenAuthentication2Filter;
import com.practice.lms.handler.MyAccessDeniedHandler;
import com.practice.lms.handler.MyAuthenticationEntryPoint;
import com.practice.lms.handler.MyLogoutSuccessHandler;
import com.practice.lms.handler.TokenLogoutHandler;
import com.practice.lms.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author will.tuo
 * @date 2021/8/18 14:14
 */
@Configuration
public class Security2Config extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private MyAuthenticationEntryPoint myAuthenticationEntryPoint;
    @Autowired
    private MyAccessDeniedHandler myAccessDeniedHandler;
    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler;
    @Autowired
    private TokenLogoutHandler tokenLogoutHandler;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private TokenAuthentication2Filter tokenAuthentication2Filter;
    @Autowired
    private MyUsernamePasswordLoginAuthenticationFilter myAuthenticationFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //指定用哪个userDetailsService实现类
        auth.userDetailsService(userDetailsService).passwordEncoder(password());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //.anyRequest().authenticated()会将所有路径都拦截，所以我们需要把登录路径放行
                .antMatchers("/login", "/register", "/redis/**").permitAll()
                .antMatchers("/test/**", "/generate").permitAll()
                /**
                 * 书管理
                 */
                .antMatchers("/book/v1").hasRole("USER")
                .antMatchers("/book/v2").hasRole("ADMIN")
                /**
                 * 权限管理
                 */
                .antMatchers("/api/authority/**").hasAnyRole(RoleEnum.AUTHORITY_ADMIN.getRole(), RoleEnum.SUPERADMIN.getRole())
                .anyRequest().authenticated()
                .and()
                // 允许注销操作
                .logout().logoutUrl("/logout").permitAll()
                // 注销操作处理器
                .addLogoutHandler(tokenLogoutHandler)
                // 注销成功处理器
                .logoutSuccessHandler(myLogoutSuccessHandler)
                // 登出后删除cookie
                .deleteCookies("JSESSIONID")
                .and()
                //未登录访问权限异常
                .exceptionHandling()
                .accessDeniedHandler(myAccessDeniedHandler)
                .authenticationEntryPoint(myAuthenticationEntryPoint);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //禁用缓存
        http.headers().cacheControl();
        http.addFilterAt(myAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(tokenAuthentication2Filter, myAuthenticationFilter.getClass()).httpBasic();
        http.csrf().disable();
    }

    @Bean
    PasswordEncoder password() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
