package com.practice.lms.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动Spring对AspectJ的支持
 * @author will.tuo
 * @date 2021/8/20 13:13
 */
@Configuration
@ComponentScan("com.practice.lms")
@EnableAspectJAutoProxy
public class AspectConfig {
}
