//package com.practice.lms.entity;
//
//import lombok.Data;
//
//import java.util.List;
///**
// * @author will.tuo
// */
//@Data
//public class MyUserDetails extends Users {
//    /**
//     * 用户额外权限列表，（有添加以及禁用）
//     */
//    private List<Authority> authorities;
//    /**
//     * 角色列表
//     */
//    private List<Role> roles;
//    /**
//     * 根据额外权限列表以及角色列表，生成的总共的权限集合
//     */
//    private List<String> authoritiyList;
//}
