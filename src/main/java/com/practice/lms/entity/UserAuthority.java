package com.practice.lms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/8/20 8:52
 * 用户权限表
 */
@Data
@TableName("user_authority")
public class UserAuthority {
    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    private int userId;
    private int authorityId;
    private String authStatus;
}
