package com.practice.lms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/8/20 8:51
 * 用戶角色关系表
 */
@Data
@AllArgsConstructor
@TableName("user_role")
public class UserRole {
    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    private int userId;
    private int roleId;
    public UserRole(int userId,int roleId){
        this.roleId = roleId;
        this.userId = userId;
    }
}
