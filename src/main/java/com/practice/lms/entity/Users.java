package com.practice.lms.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/8/18 14:37
 */
@Data
@TableName("user")
public class Users {
    @TableId(value = "id",type = IdType.AUTO)
    protected Integer id;
    protected String username;
    private String password;
    protected String realname;
    protected int age;
    protected String sex;
    protected String phonenumber;
}
