package com.practice.lms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 */
@Data
@TableName("authority")
public class Authority {
    @TableId
    private Integer authorityId;
    private String authority;
}
