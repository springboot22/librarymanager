package com.practice.lms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

@Data
@TableName("book")
public class Book {
    /**
     * 书籍id
     */
    @TableId(value = "book_id",type = IdType.AUTO)
    private Integer bookId;
    /**
     * 书籍名称
     */
    private String bookName;
    /**
     * 作者名
     */
    private String authorName;
    /**
     * 书籍状态
     */
    private String bookStatus;
    /**
     * 借阅者id
     */
    private Integer borrowerId;
    /***
     * 存馆时间
     */
    private Timestamp storageTime;
    /**
     * 借走的时间
     */
    private Timestamp borroweredTime;

}
