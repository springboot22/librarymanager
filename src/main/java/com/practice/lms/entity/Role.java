package com.practice.lms.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Set;

/**
 * @author will.tuo
 */
@Data
@TableName("role")
public class Role {
    @TableId
    private Integer roleId;
    private String role;
    private Set<Authority> authorities;
}
