package com.practice.lms.filter.old;


import com.fasterxml.jackson.databind.ObjectMapper;
//import com.practice.lms.entity.LoginUserReq;
import com.practice.lms.entity.Users;
import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import com.practice.lms.response.ResultCode;
import com.practice.lms.utils.JwtUtil;

import com.practice.lms.utils.RedisUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author will.tuo
 * @date 2021/8/19 14:52
 * 对登录信息的验证
 *      这里面的AuthenticationManager 是Spring管理的。
 *      TokenManager是我们自己的，对token进行管理。
 */
public class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private JSONAuthentication jsonAuthentication;
    private RedisUtil redisUtil;
    public TokenLoginFilter(AuthenticationManager authenticationManager,RedisUtil redisUtil) {
        this.authenticationManager = authenticationManager;
        this.setPostOnly(false);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login"));
        jsonAuthentication = new JSONAuthentication();
        this.redisUtil = redisUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        try {
            Users user = new ObjectMapper().readValue(req.getInputStream(), Users.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        /**
         * 将token存入 redis
         */
        User user = (User)auth.getPrincipal();
        String username = user.getUsername();
        String token = JwtUtil.generateToken(username);

        boolean su = redisUtil.hset(username,"token",token);
        Result result = Result
                .error(ResultCode.SUCCESS.getCode())
                .message("TokenLoginFilte成功")
                .setDate(JwtUtil.HEADER,token)
                .setDate("token缓存成功",su);
        jsonAuthentication.WriteJSON(request,response,result);
    }

    /**
     * 验证无误
     * @param request
     * @param response
     * @param e
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException e) throws IOException, ServletException {
        Result result = Result.error(ResultCode.FAILURE.getCode()).message("TokenLoginFilter验证失败");
        jsonAuthentication.WriteJSON(request,response,result);
    }
}
