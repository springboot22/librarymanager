package com.practice.lms.filter;

import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import com.practice.lms.service.AuthorityManagerService;
import com.practice.lms.utils.JwtUtil;
import com.practice.lms.utils.RedisUtil;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author will.tuo
 * @date 2021/8/19 14:53
 */
@Slf4j
@Component
public class TokenAuthentication2Filter extends OncePerRequestFilter {
    @Qualifier("JSONAuthentication")
    @Autowired
    private JSONAuthentication jsonAuthentication;
    @Autowired
    private AuthorityManagerService authorityManagerService;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
        Result result;
        String token = req.getHeader(JwtUtil.HEADER);

        if (token == null) {
            chain.doFilter(req, res);
            return;
        }
        try {
            Claims claims = JwtUtil.getClaimsFromToken(token);
            if (claims == null) {
                throw new JwtException("Token异常");
            }
            if (JwtUtil.isTokenExpired(claims)) {
                throw new JwtException("token已经过期");
            }
            String userName = claims.getSubject();
            if(!token.equals(redisUtil.hget(userName,"token"))){
                result = Result.error(123).message("Token匹配失败，请重新登陆");
                jsonAuthentication.WriteJSON(req, res, result);
                return;
            }
            if (!StringUtils.isEmpty(userName)) {
                String au = authorityManagerService.getAuthorityCollection(userName);
                log.info("auths.toString()"+au);
                List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList(au);
                log.info("auths.toString()"+auths.toString());
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName, token, auths);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                //log.info(res.get);

                chain.doFilter(req, res);
            }
        } catch (ExpiredJwtException e) {
            result = Result.error(123).message("Token已过期");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("Token已过期: {} " + e);
        } catch (UnsupportedJwtException e) {
            result = Result.error(123).message("Token格式错误");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("Token格式错误: {} " + e);
        } catch (MalformedJwtException e) {
            result = Result.error(123).message("Token没有被正确构造");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("Token没有被正确构造: {} " + e);
        } catch (SignatureException e) {
            result = Result.error(123).message("Token签名失败");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("签名失败: {} " + e);
        } catch (IllegalArgumentException e) {
            result = Result.error(123).message("Token非法参数异常");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("非法参数异常: {} " + e);
        } catch (JwtException e) {
            result = Result.error(123).message("Invalid Token");
            jsonAuthentication.WriteJSON(req, res, result);
            logger.error("Invalid Token " + e.getMessage());
        }catch (Exception e) {
            logger.error("其他问题" + e.getMessage());
        }
    }
}
