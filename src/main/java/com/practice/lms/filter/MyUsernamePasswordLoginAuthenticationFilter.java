package com.practice.lms.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.practice.lms.entity.Users;
import com.practice.lms.handler.JSONAuthentication;
import com.practice.lms.response.Result;
import com.practice.lms.response.ResultCode;
import com.practice.lms.utils.JwtUtil;
import com.practice.lms.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 登陆验证
 */
@Component
public class MyUsernamePasswordLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    @Autowired
    private RedisUtil redisUtil;
    @Qualifier("JSONAuthentication")
    @Autowired
    private JSONAuthentication jsonAuthentication;
    private boolean postOnly = true;

    public MyUsernamePasswordLoginAuthenticationFilter() {
        super(new AntPathRequestMatcher("/login", "POST"));
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException {
        if (postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        Users user = new ObjectMapper().readValue(request.getInputStream(), Users.class);
        return super.getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(),new ArrayList<>()));
    }
    @Autowired
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        User user = (User)authResult.getPrincipal();
        String username = user.getUsername();
        String token = JwtUtil.generateToken(username);

        boolean su = redisUtil.hset(username,"token",token);
        Result result = Result
                .error(ResultCode.SUCCESS.getCode())
                .message("TokenLoginFilte成功")
                .setDate(JwtUtil.HEADER,token)
                .setDate("token缓存成功",su);
        jsonAuthentication.WriteJSON(request,response,result);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        Result result = Result.error(ResultCode.FAILURE.getCode()).message("TokenLoginFilter验证失败");
        jsonAuthentication.WriteJSON(request,response,result);
    }
}
