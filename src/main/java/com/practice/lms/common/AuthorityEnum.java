package com.practice.lms.common;
/**
 * @author will.tuo
 * @date 2021/8/20 10:51
 */
public enum AuthorityEnum {
    /**
     * 权限枚举
     */
    ALL(1,"ALL"),
    BOOK_BORROW(2,"BOOK_BORROW"),
    AUTHORITY_MANAGER(3,"AUTHORITY_MANAGER"),
    BOOK_SELECT(4,"BOOK_SELECT"),
    USER_LOGIN(5,"USER_LOGIN"),
    BOOK_HANDLE(6,"BOOK_HANDLE"),
    DEFAULT(0,"DEFAULT");
    private final int id;
    private final String authority;
    AuthorityEnum(int id,String authority){
        this.id = id;
        this.authority = authority;
    }
    public int getId() {
        return id;
    }
    public String getAuthority() {
        return authority;
    }
    public static AuthorityEnum getAuthorityById(int authorityId){
        switch (authorityId){
            case 1:
                return AuthorityEnum.ALL;
            case 2:
                return BOOK_BORROW;
            case 3:
                return AUTHORITY_MANAGER;
            case 4:
                return BOOK_SELECT;
            case 5:
                return USER_LOGIN;
            case 6:
                return BOOK_HANDLE;
            default:
                return DEFAULT;
        }
    }
}
