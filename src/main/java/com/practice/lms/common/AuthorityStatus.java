package com.practice.lms.common;

/**
 * @author will.tuo
 */

public enum AuthorityStatus {
    /**
     * 对用户禁用的权限
     */
    FORBIDDEN("FORBIDDEN"),
    /**
     * 为用户添加的权限
     */
    ADD("ADD");
    private String status;
    AuthorityStatus(String status){
        this.status = status;
    }
    public String getStatus(){return this.status;}
}
