package com.practice.lms.common;

/**
 * @author will.tuo
 */

public enum BookStatus {
    /**
     * 书籍状态
     */
    FREE("free"),
    BORROWED("borrowed"),
    DELETED("deleted");
    private String status;
    BookStatus(String status){
        this.status = status;
    }
    public String getStatus(){return this.status;}
}
