package com.practice.lms.common;

/**
 * @author will.tuo
 * @date 2021/8/20 10:15
 */
public enum RoleEnum {
    /**
     * 角色以及角色ID
     */
    SUPERADMIN(1,"SUPERADMIN"),
    AUTHORITY_ADMIN(2,"AUTHORITY_ADMIN"),
    ADMIN(3,"ADMIN"),
    USER(4,"USER");
    private int roleId;
    private String role;
    RoleEnum(int roleId, String role){
        this.role = role;
        this.roleId = roleId;
    }
    public int getRoleId(){return this.roleId;}

    public String getRole() {
        return this.role;
    }

    /**
     * 根据id 获取对应的角色枚举对象
     * @param roleId
     * @return
     */
    public static RoleEnum getRoleEnum(int roleId){
        switch (roleId){
            case 1:
                return SUPERADMIN;
            case 2:
                return AUTHORITY_ADMIN;
            case 3:
                return ADMIN;
            default:
                return USER;
        }
    }
}
