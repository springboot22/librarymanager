package com.practice.lms.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.practice.lms.annocation.Mylog;
import com.practice.lms.common.RoleEnum;
import com.practice.lms.entity.UserRole;
import com.practice.lms.entity.Users;
import com.practice.lms.response.Result;
import com.practice.lms.service.UserManagerService;
import com.practice.lms.service.impl.UserRoleServiceImpl;
import com.practice.lms.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author will.tuo
 * @date 2021/8/20 9:16
 */
@RestController
@CrossOrigin
public class LoginController {
    @Autowired
    UserServiceImpl userService;
    @Autowired
    UserManagerService userManagerService;
    @Autowired
    UserRoleServiceImpl userRoleService;
    /**
     * 注册时，不仅需要存入用户信息，还需要分配基本的User角色
     * 注册时会遇到的问题：
     *      用户名已被注册
     * @param users
     * @return
     */
    @Mylog
    @RequestMapping("/register")
    public Result register(@RequestBody Users users){
        if(userManagerService.isExistUser(users.getUsername())){
            return Result.error(154).message("该用户名已被注册");
        }
        userService.save(users);
        Users users1 = userManagerService.selectUserByUsername(users.getUsername());
        UserRole userRole = new UserRole(users1.getId(),RoleEnum.USER.getRoleId());
        userRoleService.save(userRole);
        return Result.ok().message("注册成功");
    }
}
