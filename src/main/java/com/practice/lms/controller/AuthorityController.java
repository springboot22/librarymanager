package com.practice.lms.controller;

import com.practice.lms.entity.Users;
import com.practice.lms.response.Result;
import com.practice.lms.service.AuthorityManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 权限管理
 * @author will.tuo
 */
@RestController
@RequestMapping("/api/authority")
public class AuthorityController {
    /**
     * 权限管理
     */
    @Autowired
    AuthorityManagerService ams;
    /**
     * pass
     * 查询用户角色
     */
    @RequestMapping("/selectRole")
    public Result selectRoleByUsername(@RequestBody Users user){
        return Result.ok()
                .message("角色查询成功")
                .setDate("操作用户",user.getUsername())
                .setDate("role",ams.getUserRole(user.getUsername()));
    }
    /**
     * pass
     * 查询用户的额外权限
     */
    @RequestMapping("/selectAuthoritis")
    public Result selectAuthoritisByUsername(@RequestBody Users user){
        return Result.ok()
                .message("操作成功")
                .setDate("authorities",ams.getAuthoritiesForUser(user.getUsername()));
    }
    /**
     * pass
     * 更改用户角色
     */
    @RequestMapping("/updateUserRole")
    public Result updateUserRole(@RequestBody Map<String,Object> map){
        return Result.ok()
                .message("updateUserRole操作成功")
                .setDate("设置成功",ams.setUserRole(map.get("username").toString(),Integer.parseInt(map.get("roleid").toString())));
    }

    /**
     * pass
     * 添加权限
     */
    @RequestMapping("/addAuthoritiy")
    public Result addAuthoritiy(@RequestBody Map<String,Object> map){
        return Result.ok().message("添加成功")
                .setDate("添加成功",ams.addUserAuthority(map.get("username").toString(),Integer.parseInt(map.get("authorityid").toString())));
    }

    /**
     * pass
     * 移除权限
     */
    @RequestMapping("/removeAuthority")
    public Result removeAuthority(@RequestBody Map<String,Object> map){

        return Result.ok().message("移除权限成功")
                .setDate("移除成功",ams.removeUserAuthority(map.get("username").toString(),Integer.parseInt(map.get("authorityid").toString())));
    }

    /**
     * 禁用权限
     */
    @RequestMapping("/forbiddenAuthority")
    public Result forbiddenAuthority(@RequestBody Map<String,Object> map){
        return Result.ok().message("禁用权限成功")
                .setDate("移除成功",ams.forbiddenUserAuthority(map.get("username").toString(),Integer.parseInt(map.get("authorityid").toString())));
    }

}
