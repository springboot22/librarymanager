package com.practice.lms.controller;

import com.practice.lms.common.BookStatus;
import com.practice.lms.entity.Book;
import com.practice.lms.response.Result;
import com.practice.lms.service.BookManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * 借书
 * 还书
 * 查询书籍
 * @author will.tuo
 */
@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookManagerService bookManagerService;
    /**
     * pass
     * 全部查询
     * @return
     */
    @RequestMapping("/v1/selectAllBooks")
    public Result getBooksByName(){
        return Result.ok().message("查询成功")
                .setDate("书籍列表", bookManagerService.selectAll());
    }
    /**
     * pass
     * 根据书名查询书籍（关键字）
     * @return
     */
    @RequestMapping("/v1/selectBooksByName")
    public Result getBooksByName(@RequestBody Map<String,Object> map) {
        return Result.ok().message("查询成功")
                .setDate("书籍列表", bookManagerService.selectBooksByKey(map.get("key").toString()));
    }

    /**
     * 借书 pass
     * 这里还可以对 已经时节约状态的书籍，进行不能借阅的提示
     * @return
     */
    @RequestMapping("/v1/borrow")
    public Result borrowBook(@RequestBody Map<String,Object> map){
        bookManagerService.borrow((int)map.get("userid"),(int)map.get("bookid"));

        return Result.ok().message("查询成功")
                .setDate("书籍列表", bookManagerService.selectAll());
    }

    /**
     * 还书 pass
     * @return
     */
    @RequestMapping("/v1/return")
    public Result returnBook(@RequestBody Map<String,Object> map){
        bookManagerService.returnBook((int)map.get("userid"),(int)map.get("bookid"));
        return Result.ok().message("查询成功")
                .setDate("书籍列表", bookManagerService.selectAll());
    }
    /**
     * 添加 pass
     * @return
     */
    @RequestMapping("/v2/addBook")
    public Result addBook(@RequestBody Map<String,Object> map){
        Book book = new Book();
        book.setBookName(map.get("bookname").toString());
        book.setBookStatus(BookStatus.FREE.getStatus());
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateStr = now.format(ofPattern);
        Timestamp timestamp = Timestamp.valueOf(dateStr);
        book.setStorageTime(timestamp);
        bookManagerService.addBook(book);
        return Result.ok()
                .message("插入成功");
    }
    /**
     * pass
     * 删除
     * @return
     */
    @RequestMapping("/v2/deleteBook")
    public Result deleteBook(@RequestBody Map<String,Object> map){
        bookManagerService.deleteBook(Integer.parseInt(map.get("bookid").toString()));
        return Result.ok()
                .message("删除成功");
    }

}
