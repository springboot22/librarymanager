package com.practice.lms.controller;

import com.practice.lms.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/8/9 10:17
 * Redis 使用测试
 */
@RestController
@RequestMapping("/redis")
public class RedisController {
    @Autowired
    private RedisUtil redisUtil;
    @RequestMapping("/get")
    public String get(){
        redisUtil.hset("user","name",1);
        System.out.println(">>>>>>>>>>hn"+ redisUtil.hget("user","name"));
        return ">>>>>>>>>>hn"+ redisUtil.hget("user","name");
    }
    @RequestMapping("/")
    public String getIndex(){
        return "hello";
    }
}
