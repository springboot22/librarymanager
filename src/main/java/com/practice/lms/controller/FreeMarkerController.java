package com.practice.lms.controller;


import com.practice.lms.utils.FreeMarkerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author will.tuo
 * FreeMarker 使用测试
 */
@RestController
@Slf4j
public class FreeMarkerController {

    @Autowired
    FreeMarkerUtil freeMarkerUtil;
    private final String projectRootSrc = System.getProperty("user.dir");
    /**
     * 生成代码接口
     * @para tableName 表名
     * @para saveUrl 生成文件路径
     * @para basePackageUrl 生成上级包名
     */
    @RequestMapping("/generate")
    public String createEntity(@RequestBody Map<String,String> map) throws Exception {

        String tableName = map.get("tablename");
        String saveUrl = map.get("saveUrl");
        String basePackageUrl = map.get("basepackageurl");

        //生成路径，根据实际情况修改即可
        saveUrl = saveUrl == null ? projectRootSrc+"/src/main/resources/output": saveUrl;
        log.info(projectRootSrc);
        //生成文件包名，根据实际情况修改即可
        basePackageUrl = basePackageUrl == null? "com.practice.lms": basePackageUrl;
        //bean类名
        String entityName = freeMarkerUtil.getEntityName(tableName);
        //封装参数
        Map<String, Object> root = new HashMap<>();
        root.put("basePackageUrl", basePackageUrl);
        //表参数
        root.put("tableName", tableName);
        root.put("entityName", entityName);
        root.put("entityNameLower", freeMarkerUtil.getEntityNameLower(tableName));
        root.put("columns", freeMarkerUtil.getDataInfo(tableName));
        // 生成bean 对不同的文件 加上不同的路径后缀即可
        freeMarkerUtil.generate(root,"entity.ftl",saveUrl,entityName+".java");
        // 生成dao
        freeMarkerUtil.generate(root,"dao.ftl",saveUrl,entityName+"Dao.java");
        // 生成mapper
        freeMarkerUtil.generate(root,"mapper.ftl",saveUrl,entityName+"Mapper.xml");
        // 生成controller
        freeMarkerUtil.generate(root,"controller.ftl",saveUrl,entityName+"Controller.java");
        //生成service
        freeMarkerUtil.generate(root,"service.ftl",saveUrl,entityName+"Service.java");
        //生成serviceImpl
        freeMarkerUtil.generate(root,"serviceImpl.ftl",saveUrl,entityName+"ServiceImpl.java");
        return "生成成功";
    }
}
