package com.practice.lms.controller;

import com.practice.lms.annocation.Mylog;
import com.practice.lms.aware.ApplicationContextProvider;
import com.practice.lms.common.AuthorityEnum;
import com.practice.lms.response.Result;
import com.practice.lms.service.AuthorityManagerService;
import com.practice.lms.service.UserManagerService;
import com.practice.lms.service.impl.UserAuthorityServiceImpl;
import com.practice.lms.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 *  测试 控制器
 * @author will.tuo
 * @date 2021/8/19 10:05
 */
@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {

    @Autowired
    AuthorityManagerService ams;
    @Autowired
    UserManagerService ums;
    @Autowired
    UserAuthorityServiceImpl userAuthorityService;
    @Autowired
    ApplicationContextProvider applicationContext;
    @RequestMapping("/1")
    @Mylog(level = "info")
    public String Login(){
        return "vjnfi";
    }

    @RequestMapping("/12")
    public String Login2(){
        return "vjnfighjmn";
    }

    @RequestMapping("/get1")
    public String get1(){
        return "get1";
    }

    @RequestMapping("/get2")
    public String get2(){
        return "get2";
    }
    @RequestMapping("/get3")
    public void get3(){
        System.out.println(JwtUtil.generateToken("ndecuiofh"));
    }
    @RequestMapping("/getAC")
    public Object getAC(){
        TestController tc = applicationContext.getBean(TestController.class);
        return tc.getClass().getAnnotations();
    }
    /**
     * 权限test
     */
    @RequestMapping("/s1")
    public Result s1(@RequestBody Map<String,Object> map){
        String username = map.get("username").toString();


//        Users user = ums.selectUserByUsername(username);
//        List<UserAuthority> userAuthorities = userAuthorityService.list().stream()
//                .filter(w->w.getUserId() == user.getId())
//                .collect(Collectors.toList());
//        String a = ams.getAuthorityCollection(username);
//        log.info(a);
//        List<AuthorityEnum> aes = userAuthorities.stream().map((e)-> AuthorityEnum.getAuthorityById(e.getAuthorityId())).collect(Collectors.toList());
//        List<String> ass = aes.stream().map(AuthorityEnum::getAuthority).collect(Collectors.toList());


        List<AuthorityEnum> as = ams.getAuthoritiesForUser(username);
        StringBuilder sb = new StringBuilder();
        sb.append("ABB,ROLE_");
        log.info("vvvvvvvv"+sb.toString());
        sb.append(",");
        String ass = as.stream().map(AuthorityEnum::getAuthority).reduce((x1,x2)->x1+","+x2).get();
        sb.append(ass);
        log.info("vvvvvvvv"+sb.toString());
        return Result.ok().message("列表")
                .setDate("1",ams.getUserRole(username))
                .setDate("2",ams.getAuthoritiesForUser(username))
                .setDate("9",ams.getAuthorityCollection(username));
//                .setDate("3",userAuthorities)
//                .setDate("4",userAuthorities.stream().filter(w-> !w.getAuthStatus().equals(AuthorityStatus.FORBIDDEN.getStatus())))
//                .setDate("5",aes)
//                .setDate("6",aes.stream().map(AuthorityEnum::getAuthority))
//                .setDate("7",a);
    }
}
