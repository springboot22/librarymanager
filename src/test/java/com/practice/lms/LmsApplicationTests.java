package com.practice.lms;

import com.practice.lms.common.AuthorityEnum;
import com.practice.lms.common.RoleEnum;
import com.practice.lms.entity.Authority;
import com.practice.lms.entity.UserRole;
import com.practice.lms.entity.Users;
import com.practice.lms.service.AuthorityManagerService;
import com.practice.lms.service.UserManagerService;
import com.practice.lms.service.impl.UserRoleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
class LmsApplicationTests {
    @Autowired
    UserManagerService userManagerService;
    @Autowired
    UserRoleServiceImpl userRoleService;
    @Autowired
    AuthorityManagerService ams;
    @Test
    void contextLoads() {
        String userName = "tm";
        RoleEnum role = ams.getUserRole(userName);
        List<AuthorityEnum> as = ams.getAuthoritiesForUser(userName);

        log.info(role.toString());
        log.info(ams.getAuthorityCollection(userName));
//        userRoleService.list();
    }
    @Test
    void test(){
        StringBuilder sb = new StringBuilder();
        sb.append("jdfi");
        sb.append("nvdksl");
        log.info(sb.toString());
        sb.append("ndsl");
        log.info(sb.toString());
    }

}
