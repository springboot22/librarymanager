## LibraryManagerSystem

[TOC]

### 说明

- 有一些内容只是象征性的用了，目前的理解还没办法涉及一些比较真实的用法。比如Bean初始化过程中相关的方法应用。

- 类图和时序图还未添加

### 一、系统技术路线

#### 1、涉及技术

Spring Boot+Spring Security + Spring AOP + Mybatis-Plus + Redis + JWT + FreeMaker+(Slf4j+log4j)

#### 2、依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.5.3</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.practice</groupId>
    <artifactId>lms</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>lms</name>
    <description>Demo project for Spring Boot</description>
    <properties>
        <java.version>1.8</java.version>
    </properties>
    <dependencies>
        <!-- Redis-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
         <!-- Spring security-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
         <!-- Spring Boot-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
         <!-- Spring AOP-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
        </dependency>
         <!-- MyBatis-Plus-->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.2.0</version>
        </dependency>
         <!-- freemarker-->
        <dependency>
            <groupId>org.freemarker</groupId>
            <artifactId>freemarker</artifactId>
            <version>2.3.28</version>
        </dependency>
         <!-- MySQL-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
          <!-- lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <!-- fastjson-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.6</version>
        </dependency>
        <!-- slf4j log4j-->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.25</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.9.1</version>
        </dependency>
        <!-- jjwt-->
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt</artifactId>
            <version>0.6.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                        </exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

#### 3、使用情况

- Spring Boot

  构建Web服务端程序

-  SpringAOP

  - 配合日志完成controller的方法调用日志

- Spring Security +  JWT + Redis

  - Spring Security +  JWT 二者配合共同完成基于Spring Security的权限验证机制和Token登陆用户标识验证

  - JWT + Redis 完成用户登陆的token缓存对比验证

-  Mybatis-Plus 

  简化数据库操作

-  FreeMaker

  - 基于数据库表的Bean\DAO \Controller\Service\ServiceImpl文件生成
  - 前端传入对应需要生成的表名即可

- (Slf4j+log4j)

  日志管理

- 一些用法
  - @PostConstruct
  - afterPropertiesSet
  - ApplicationContext
  - 自定义注解

### 二、功能需求描述

1、图书管理：

- 新增
- 删除
- 修改书籍信息
- 查询书籍
- 借阅、还书

2、角色管理：查询，更改

3、角色、权限管理：拥有某个权限的角色，可以为其他用户添加额外权限；

- 查詢用戶角色
- 查詢用戶權限

- 更改用户角色

- 添加用户权限

- 删除用户权限

- 禁用用户权限

4、用户管理:角色、权限、登陆注册。

- 登录
- 注册
- 登出

5、日志管理：使用Slf4j+log4j

6、使用模板引擎Freemaker一键生成代码文件。

### 三、用户角色与权限

- 理解

1、用户User一注册就有一个最基本的角色    

2、每个角色Role，拥有不同的权限集合 Authorities

超级管理员、普通管理员、拥有部分超级管理员的权限的普通管理员。

用户组Group(就是一种临时角色)

3、每个用户在不同情况下，可以被管理员开启、禁用某个权限

4、一个用户的所有权限，包括：他所属于角色的权限加额外分配的权限。

- 权限以及角色分配：
  - 普通权限（user）：书籍查询(BOOK_SELECT)、登录(USER_LOGIN)、借阅 (BOOK_BORROW) 
  - 管理员（admin）：书籍操作  BOOK_HANDLE
  - 权限管理员（authorityAdmin）：为除了超级管理员之外的所有用户管理权限(AUTHORITY_MANAGER)
  - 超级管理员（superAdmin）：一切操作都放行（ALL）。

### 四、结构设计

#### 1、数据库

##### 模型图

![image-20210823084729251](image/image-20210823084729251.png)

##### ER图

![image-20210823091148916](image/image-20210823091148916.png)

#### 2、类图

##### entity

![image-20210823092245879](image/image-20210823092245879.png)

##### common

![image-20210823092420512](image/image-20210823092420512.png)

#### 3、时序图



### 五、详细设计

#### 1、项目结构

![image-20210822231958422](image/image-20210822231958422.png)

#### 2、utils工具类

- FreeMarkerUtil freemaker代码文件生成
- JwtUtils Token生成验证
- RedisUtil redisTemplate的封装

#### 3、Spring Security使用

- Security2Config配置文件
  - 配置过滤器、处理器
  
  - 拦截路径与权限分配
  
    ```java
    /**
     * @author will.tuo
     * @date 2021/8/18 14:14
     */
    @Configuration
    public class Security2Config extends WebSecurityConfigurerAdapter {
        @Autowired
        private UserDetailsService userDetailsService
        @Autowired
        private MyAuthenticationEntryPoint myAuthenticationEntryPoint;
        @Autowired
        private MyAccessDeniedHandler myAccessDeniedHandler;
        @Autowired
        private MyLogoutSuccessHandler myLogoutSuccessHandler;
        @Autowired
        private TokenLogoutHandler tokenLogoutHandler;
        @Autowired
        private AuthorityManagerService authorityManagerService;
        @Autowired
        RedisUtil redisUtil;
        @Autowired
        private TokenAuthentication2Filter tokenAuthentication2Filter;
        @Autowired
        private MyUsernamePasswordLoginAuthenticationFilter myAuthenticationFilter;
        /**
         * 登录被挤下线的处理
         */
        @Autowired
        private MyExpiredSessionStrategy myExpiredSessionStrategy;
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            //指定用哪个userDetailsService实现类
            auth.userDetailsService(userDetailsService).passwordEncoder(password());
        }
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    //.anyRequest().authenticated()会将所有路径都拦截，所以我们需要把登录路径放行
                    .antMatchers("/login","/register","/redis/**").permitAll()
                    .antMatchers("/test/**","/generate").permitAll()
                    .antMatchers("/book/v1").hasRole("USER")
                    .antMatchers("/book/v2").hasRole("ADMIN")          	 	       .antMatchers("/api/authority/**").hasAnyRole(RoleEnum.AUTHORITY_ADMIN.getRole(),RoleEnum.SUPERADMIN.getRole())   
                    .anyRequest().authenticated()
                    .and()
                    //允许注销操作
                    .logout().logoutUrl("/logout").permitAll()
                    //注销操作处理器
                    .addLogoutHandler(tokenLogoutHandler)
                    //注销成功处理器
                    .logoutSuccessHandler(myLogoutSuccessHandler)
                    //登出后删除cookie
                    .deleteCookies("JSESSIONID")
                    .and()
                    //未登录访问权限异常
                    .exceptionHandling()
                    .accessDeniedHandler(myAccessDeniedHandler)
                    .authenticationEntryPoint(myAuthenticationEntryPoint);
            //禁用session
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            //禁用缓存
            http.headers().cacheControl();
            http.addFilterAt(myAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
            .addFilterAfter(tokenAuthentication2Filter,myAuthenticationFilter.getClass()).httpBasic();
            http.csrf().disable();
        }
    
        @Bean
        PasswordEncoder password(){
            return new BCryptPasswordEncoder();
        }
        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }
    }
    ```
- com.practice.lms.filter
  - old包中的文件弃用，可不看
  - MyUsernamePasswordLoginAuthenticationFilter用于登陆的过滤器
    - 登录成功与否处理
      - successfulAuthentication
      - unsuccessfulAuthentication
  - TokenAuthentication2Filter
    - 用户权限验证过滤器（主要是讲用户权限查询出来，封装到指定对象）
    - Token验证

- handler包

  - JSONAuthentication 权限验证过程中负责发送response
  - MyAccessDeniedHandler 用户无权限的处理器
  - MyAuthenticationEntryPoint 匿名(未登录)用户访问无权限异常
  - MyLogoutSuccessHandler 登出操作成功处理器
  - TokenLogoutHandler 登出操作处理器

#### 4、Service

- 数据库Mybatis-Plus的相关Service
  - 重复写法，继承、引用Mybatis-Plus对应类或接口
    - Service.iservice
    - Service.Impl

- 业务处理相关Service

  - AuthorityManagerService 权限验证服务

    ```java
    @Slf4j
    @Service
    public class AuthorityManagerService {
        @Autowired
        UserManagerService userManagerService;
        @Autowired
        UserRoleServiceImpl userRoleService;
        @Autowired
        RoleServiceImpl roleService;
        @Autowired
        UserAuthorityServiceImpl userAuthorityService;
        /**
         * 查询用户权限集合
         * 这里需要将role 和 authority翻译成一个集合。
         * @return
         */
        public List<AuthorityEnum> getAuthoritiesForUser(String username){
            Users user = userManagerService.selectUserByUsername(username);
            List<UserAuthority> userAuthorities = userAuthorityService.list().stream()
                    .filter(w->w.getUserId() == user.getId())
                    .filter(w1-> !w1.getAuthStatus().equals(AuthorityStatus.FORBIDDEN.getStatus()))
                    .collect(Collectors.toList());
            return userAuthorities.stream()
                    .map((e)-> AuthorityEnum.getAuthorityById(e.getAuthorityId()))
                    .collect(Collectors.toList());
        }
    
        /**
         * 用户角色查询
         * @param username
         * @return
         */
        public RoleEnum getUserRole(String username){
            Users user = userManagerService.selectUserByUsername(username);
            UserRole userRole = userRoleService.list().stream().filter(w->w.getUserId()==user.getId()).findFirst().get();
            return RoleEnum.getRoleEnum(userRole.getRoleId());
        }
    
        /**
         * 设置用户角色
         * @return
         */
        public boolean setUserRole(String username,int roleid){
            Users user = userManagerService.selectUserByUsername(username);
            UpdateWrapper<UserRole> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("role_id",roleid).eq("user_id",user.getId());
            return userRoleService.update(updateWrapper);
        }
        /**
         * 添加用户权限
         * @param username
         * @return
         */
        public boolean addUserAuthority(String username, int authorityid){
            Users user = userManagerService.selectUserByUsername(username);
            UserAuthority userAuthority = new UserAuthority();
            userAuthority.setUserId(user.getId());
            userAuthority.setAuthorityId(authorityid);
            userAuthority.setAuthStatus(AuthorityStatus.ADD.getStatus());
            return userAuthorityService.save(userAuthority);
        }
    
        /**
         * 移除用户权限
         * @return
         */
        public boolean removeUserAuthority(String username, int authorityid){
            Users user = userManagerService.selectUserByUsername(username);
            LambdaQueryWrapper<UserAuthority> lambdaQueryWrapper = Wrappers.lambdaQuery();
            lambdaQueryWrapper.eq(UserAuthority::getUserId,user.getId())
                    .eq(UserAuthority::getAuthorityId,authorityid);
            return userAuthorityService.remove(lambdaQueryWrapper);
        }
    
        /**
         * 禁用用户权限
         * 设置权限表里某个权限状态为 FORBIDDEN
         * @return
         */
        public boolean forbiddenUserAuthority(String username, int authorityid){
            Users user = userManagerService.selectUserByUsername(username);
            UpdateWrapper<UserAuthority> updateWrapper = new UpdateWrapper<>();
           updateWrapper.set("auth_status",AuthorityStatus.FORBIDDEN.getStatus())
                    .eq("user_id",user.getId())
                    .eq("authority_id",authorityid);
            return userAuthorityService.update(updateWrapper);
        }
        public String getAuthorityCollection(String userName){
            System.out.println("getAuthorityCollectiondsvsfvdfbsdbdsbdg");
            StringBuilder sb = new StringBuilder();
            String userRole = getUserRole(userName).getRole();
            log.info("vvvvvvvv"+userRole);
            sb.append("ABB,ROLE_");
            log.info("vvvvvvvv"+sb.toString());
            sb.append(userRole);
            sb.append(",");
            List<AuthorityEnum> as = getAuthoritiesForUser(userName);
            String ass = as.stream().map(AuthorityEnum::getAuthority).reduce((x1,x2)->x1+","+x2).get();
            sb.append(ass);
            log.info("vvvvvvvv"+sb.toString());
            return sb.toString();
        }
    
    }
    ```

  - BookManagerService 书籍管理服务

    ```java
    @Service
    public class BookManagerService {
        @Autowired
        BookServiceImpl bookService;
        @Autowired
        BookMapper bookMapper;
        /**
         * 全部书籍列表
         * @return
         */
        public List<Book> selectAll(){
            return bookService.list();
        }
        /**
         * 关键字查询书籍列表
         * @param key
         * @return
         */
        public List<Book> selectBooksByKey(String key){
            QueryWrapper<Book> queryWrapper = new QueryWrapper<>();
            queryWrapper.like("book_name",key);
            return bookService.list(queryWrapper);
        }
    
        /**
         * 借书
         * - 设置书籍状态
         * - 设置借阅人id
         * - 设置借阅时间
         * @param userid
         * @param bookid
         * @return
         */
        public boolean borrow(int userid,int bookid){
            try{
                UpdateWrapper<Book> updateWrapper = new UpdateWrapper<>();
    
                Book book = bookService.getById(bookid);
                book.setBookStatus(BookStatus.BORROWED.getStatus());
                /**
                 * 借阅时间设置
                 */
                book.setBorrowerId(userid);
                bookMapper.updateById(book);
                return true;
            }catch (Exception e){
            }
            return false;
        }
    
        /**
         * 还书
         * - 设置书籍状态
         * - 清除借阅人id
         * - 清除借阅时间
         * @param userid
         * @param bookid
         * @return
         */
        public boolean returnBook(int userid,int bookid){
            Book book = bookService.getById(bookid);
            book.setBookStatus(BookStatus.FREE.getStatus());
            book.setBorrowerId(-1);
            /**
             * 借阅时间清空
             */
            bookMapper.updateById(book);
            return true;
        }
    
        /**
         * - 保存书籍
         * - 设置书籍入馆实时间
         * - 设置书籍初始状态
         * @param book
         * @return
         */
        public boolean addBook(Book book){
            bookService.save(book);
            return true;
        }
    
        /**
         * 更改书籍信息
         * - 更改书籍状态
         * @return
         */
        public boolean updateBookStatus(int bookid,String bookStatus){
            Book book = bookService.getById(bookid);
            book.setBookStatus(bookStatus);
            bookMapper.updateById(book);
            return true;
        }
        /**
         * 真删除
         * @param bookid
         * @return
         */
        public boolean deleteBook(int bookid){
            return bookService.removeById(bookid);
        }
    }
    ```

  - UserManagerService 用户相关

    ```java
    @Service
    @Slf4j
    public class UserManagerService {
        @Autowired
        UserServiceImpl userService;
        @Autowired
        AuthorityManagerService ams;
        public Users selectUserByUsername(String username){
            QueryWrapper<Users> wrapper = new QueryWrapper<>();
            wrapper.eq("username",username);
            return userService.getOne(wrapper);
        }
        /**
         * 通过用户名，如果能查到这个用户，就存在，如果为空则 不存在
         * @param username
         * @return
         */
        public boolean isExistUser(String username){
            return selectUserByUsername(username) != null;
        }
    }
    ```

  - MyUserDetailsService Spring Security在登陆验证时，需要进行的数据库查询的用户验证服务

  - ```java
    @Service("userDetailsService")
    public class MyUserDetailsService implements UserDetailsService {
        @Autowired
        private UserManagerService userManagerService;
        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            Users users = userManagerService.selectUserByUsername(username);
            if(users==null){
                //认证失败
                throw new UsernameNotFoundException("用户名不存在");
            }
            return new User(users.getUsername(),new BCryptPasswordEncoder().encode(users.getPassword()),new ArrayList<>());
        }
    }
    ```

#### 5、Response封装

- CustomizeResultCode 返回码与返回消息获取接口

  ```java
  public interface CustomizeResultCode {
      /**
       * 获取错误状态码
       * @return
       */
      Integer getCode();
  
      /**
       * 获取错误信息
       * @return
       */
      String getMessaage();
  }
  ```

- Result 返回数据体定义 链式编程

  ```java
  @Data
  public class Result {
      /**
       * 返回成功与否
       */
      private Boolean success;
      /**
       * 返回码
       */
      private Integer code;
      /**
       * 返回消息
       */
      private String message;
      /**
       * 返回数据体
       */
      private Map<String,Object> data = new HashMap<>();
      public Result(){}
      public static Result ok(){
          Result result = new Result();
          result.setSuccess(true);
          result.setCode(ResultCode.SUCCESS.getCode());
          result.setMessage(ResultCode.SUCCESS.getMessaage());
          return result;
      }
      public static Result error(Integer resultCode){
          Result result = new Result();
          result.setSuccess(false);
          result.setCode(resultCode);
          return result;
      }
      public Result message(String msg){
          this.setMessage(msg);
          return this;
      }
      public Result setDate(String key,Object value){
          this.data.put(key,value);
          return this;
      }
  }
  ```

- ResultCode 返回码、消息体枚举定义

  ```java
  public enum ResultCode implements CustomizeResultCode {
      /**
       *
       */
      SUCCESS(200,"成功"),
      FAILURE(444,"失败");
      private Integer code;
      private String message;
      ResultCode(Integer code,String message){
          this.code = code;
          this.message = message;
      }
      @Override
      public Integer getCode() {
          return this.code;
      }
      @Override
      public String getMessaage() {
          return this.message;
      }
  }
  ```

#### 6、common

- AuthorityEnum 权限枚举

  ```java
  ALL(1,"ALL"),
  BOOK_BORROW(2,"BOOK_BORROW"),
  AUTHORITY_MANAGER(3,"AUTHORITY_MANAGER"),
  BOOK_SELECT(4,"BOOK_SELECT"),
  USER_LOGIN(5,"USER_LOGIN"),
  BOOK_HANDLE(6,"BOOK_HANDLE"),
  DEFAULT(0,"DEFAULT");
  ```

- AuthorityStatus 权限状态枚举

  ```java
  /**
   * 对用户禁用的权限
   */
  FORBIDDEN("FORBIDDEN"),
  /**
   * 为用户添加的权限
   */
  ADD("ADD");
  ```

- BookStatus 书籍状态枚举

  ```java
  /**
   * 书籍状态
   */
  FREE("free"),
  BORROWED("borrowed"),
  DELETED("deleted");
  ```

- RepeatedlyRequestWrapper request包装器

- RoleEnum 角色枚举

  ```java
  /**
   * 角色以及角色ID
   */
  SUPERADMIN(1,"SUPERADMIN"),
  AUTHORITY_ADMIN(2,"AUTHORITY_ADMIN"),
  ADMIN(3,"ADMIN"),
  USER(4,"USER");
  ```

#### 7、ApplicationContextProvider

```java
	/**
	 * 通过setter方法注入 获取ApplicationContext
     * 上下文对象实例
     */
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
```

#### 8、SpringAOP

- 相关用法
  - @Aspect注解
  - @Pointcut
  - @Before
  - @Around
  - expression

- aop包中的LogAspect

```java
@Aspect
@Component
@Slf4j
public class LogAspect {
    @Pointcut("@annotation(com.practice.lms.annocation.Mylog)")
    public void annotationPointCut() {};

    @Before("annotationPointCut()")
    public void after(JoinPoint joinPoint) {
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Method method = ms.getMethod();
        log.info(method.getName()+"正在被调用");
    }
    @Before("execution(* com.practice.lms.controller.*Controller.*(..))")
    public void before(JoinPoint joinPoint) {
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Method method = ms.getMethod();
        log.info("方法规则拦截" + method.getName());
    }
	....
}
```

#### 9、Slf4j+log4j

引入依赖@Slf4j注解在类上，使用log方法即可

log4j.xml

#### 10、Bean初始化

构造方法- > @Autowired -> @PostConstruct->afterPropertiesSet->init-method

afterPropertiesSet

 @PostConstruct

#### 11、Freemaker

- FreeMarkerUtil工具类

  - 创建、输出内容到文件

    ```java
    /**
     * 输出到文件
     */
    public  void printFile(Map<String, Object> root, Template template, String filePath, String fileName) throws Exception  {
        pathJudgeExist(filePath);
        File file = new File(filePath, fileName );
        if(!file.exists()) {
            file.createNewFile();
        }
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
        template.process(root,  out);
        out.close();
    }
    ```

  - 驼峰命名规则格式化

    ```java
    /**
     * 下划线命名转为驼峰命名
     */
    public String underlineToHump(String para){
        StringBuilder result=new StringBuilder();
        String a[]=para.split("_");
        for(String s:a){
            if(result.length()==0){
                result.append(s);
            }else{
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }
    ```

  - 类名获取

  - 数据库类型转换成Java类型

    ```java
    /**
     * 将[数据库类型]转换成[Java类型],如果遇到没有写的类型,会出现Undefine,在后面补充即可
     */
    public  String convertToJava(String columnType){
        String result;
        switch (columnType){
            case "VARCHAR":{
                result = "String";
                break;
            }
            case "INT":{
                result = "Integer";
                break;
            }
            case "BIGINT":{
                result = "Long";
                break;
            }
            case "FLOAT":{
                result = "Float";
                break;
            }
            case "DOUBLE":{
                result = "Double";
                break;
            }
            case "DATETIME":{
                result = "Date";
                break;
            }
            case "BIT":{
                result = "Boolean";
                break;
            }
            default:{
                result = "Undefine";
                break;
            }
        }
        return result;
    }
    ```

  - 获取Table信息

    ```java
    /**
     * 获取表信息
     */
    public  List<Map<String, String>> getDataInfo(String tableName){
        // mysql查询表结构的语句,如果是其他数据库,修改此处查询语句
        String sql = "show columns from "+tableName;
        List<Map<String, Object>> sqlToMap = jdbcTemplate.queryForList(sql);
    
        List<Map<String, String>> columns = new LinkedList<>();
        for (Map map : sqlToMap) {
            Map<String, String> columnMap = new HashMap<>(16);
            // 字段名称
            String columnName = map.get("Field").toString();
            columnMap.put("columnName", columnName);
            // 字段类型
            String columnType = map.get("Type").toString().toUpperCase();
            columnType = matchResult(columnType).trim();
            columnType = convertToJava(columnType);
            columnMap.put("columnType", columnType);
            // 成员名称
            columnMap.put("entityColumnNo", underlineToHump(columnName));
            columns.add(columnMap);
        }
        return columns;
    }
    ```

  - 生成代码

    ```java
    /**
     * 生成代码
     */
    public void generate(Map<String, Object> root,String templateName,String saveUrl,String entityName) throws Exception {
        //获取模板
        Template template = freeMarker.getTemplate(templateName);
        //输出文件
        printFile(root, template, saveUrl, entityName);
    }
    ```

- resources/templates 

  - 模板文件

    ```java
    //获取模板
    Template template = freeMarker.getTemplate(templateName);
    ```

  - 模板语法

## 知识及相关资料

#### 初始化过程中的几种操作

1、afterPropertiesSet->init-method

详见：AbstractAutowireCapableBeanFactory类中的invokeInitMethods

2、构造方法- > @Autowired -> @PostConstruct

https://cloud.tencent.com/developer/article/1371264

3、总体

构造方法- > @Autowired -> @PostConstruct->afterPropertiesSet->init-method

https://blog.csdn.net/xurk0922/article/details/108036810

